package main;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.plaf.synth.SynthSeparatorUI;

import model.Animal;
import util.DatabaseUtil;

import controller.Controller;


/*public class Main {
	public static void main(String[] args) throws Exception {
		DatabaseUtil dbUtil = new DatabaseUtil();
		dbUtil.setUp();
		dbUtil.startTransaction();
		
		// Create 
		//dbUtil.createAnimal(1, "Jake", "Dog", (byte)0, 5);
		//dbUtil.createAnimal(2, "Rex", "Dog", (byte)0, 3);
		//dbUtil.createAnimal(3, "Perrie", "Parrot", (byte)1, 1);

		//dbUtil.createPersonalMedical(1, "Mihai", "Doctor");
		//dbUtil.createPersonalMedical(2, "Maria", "Asistent");
		//dbUtil.createPersonalMedical(3, "Gabriela", "Ingrijitor");
		
		//dbUtil.createProgramare(1, 2, 1, date);
		//dbUtil.createProgramare(2, 3, 2, date);
		//dbUtil.createProgramare(3, 1, 1, date);
		
		dbUtil.deletePersonalMedical(1);
		dbUtil.deletePersonalMedical(2);
		dbUtil.deletePersonalMedical(3);
		
		dbUtil.commitTransaction();
		//dbUtil.printAllAnimalsFromDB();
		//dbUtil.printAllPersonalMedicalFromDB();
		//dbUtil.printAllProgramareFromDB();
		//dbUtil.printSortedProgramareFromDB();
		dbUtil.closeEntityManager();
	}
}*/

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
	
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Login.fxml"));
        primaryStage.setTitle("PetApp");
        Scene scene = new Scene(root);
        
        //scene.getStylesheets().add("/fxml_styles/main.css");
        
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}


