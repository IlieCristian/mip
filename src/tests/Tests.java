package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.Test;

import model.Animal;
import model.History;
import model.Owner;
import model.Personalmedical;
import model.Programare;
import model.User;

class Tests {

	@Test
	void animalConstructor() {
		Animal animal = new Animal(1, "Parry", 2, 2, "Parrot", "Perush", (byte)1, 1);
		assertEquals("Parry", animal.getName());
	}
	
	@Test
	void appointmentConstructor() {
		Animal animal = new Animal(1, "Parry", 2, 2, "Parrot", "Perush", (byte)1, 1);
		assertEquals("Parrot", animal.getSpecies());
	}
	
	@Test
	void medicalPersonnelConstructor() {
		Personalmedical personal = new Personalmedical(1, "Michael", "Sanitor");
		assertEquals("Michael", personal.getName());
	}
	
	@Test
	void ownerConstructor() {
		Owner owner = new Owner(1, "Patrick", "0728536740", "patrick@gmail.com");
		assertEquals("patrick@gmail.com", owner.getEmail());
	}
	
	@Test
	void userConstructor() {
		User user = new User(1, "Admin", "pass", 1);
		assertEquals("Admin", user.getUsername());
		assertEquals("pass", user.getPassword());
	}
	
	@Test
	void animalSetters() {
		Animal animal = new Animal();
		animal.setIdAnimal(1);
		animal.setName("Parry");
		assertEquals(1, animal.getIdAnimal());
		assertEquals("Parry", animal.getName());
	}
	
	@Test
	void appointmentSetters() {
		Programare programare = new Programare();
		programare.setIdprogramare(1);
		assertEquals(1, programare.getIdprogramare());
	}
	
	@Test
	void medicalPersonnelSetters() {
		Personalmedical personal = new Personalmedical();
		personal.setJob("Nurse");
		assertEquals("Nurse", personal.getJob());
	}
	
	@Test
	void ownerSetters() {
		Owner owner = new Owner();
		owner.setEmail("patrick@gmail.com");
		assertEquals("patrick@gmail.com", owner.getEmail());
	}
	
	@Test
	void userSetters() {
		User user = new User();
		user.setUsername("Admin");
		user.setPassword("pass");
		assertEquals("Admin", user.getUsername());
		assertEquals("pass", user.getPassword());
	}
}
