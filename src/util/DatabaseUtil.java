/**
 * 
 */
package util;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.mysql.jdbc.Connection;

import manager.AnimalManager;
import manager.AppointmentManager;
import manager.HistoryManager;
import manager.MedicalPersonnelManager;
import manager.OwnerManager;
import manager.UserManager;
import model.Animal;
import model.History;
import model.Owner;
import model.Personalmedical;
import model.Programare;

/**
 * The DatabaseUtil class manage the connection and the transactions
 * made between the java program and the MySQL database
 * 
 * @author Ilie Florian-Cristian
 * @version 1.0
 * @since 2018-10-29
 * 
 */
public class DatabaseUtil {
	
	private static final DatabaseUtil instance = new DatabaseUtil();
	private static Connection connection;
	
	private DatabaseUtil() {};
	
	public static DatabaseUtil getInstance() {
		return instance;
	}
	
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
	
	AnimalManager animalManager;
	MedicalPersonnelManager medicalPersonnelManager;
	AppointmentManager appointmentManager;
	OwnerManager ownerManager;
	HistoryManager historyManager;
	UserManager userManager;

	public void setUp() throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("PetShop");
		entityManager = entityManagerFactory.createEntityManager();
		
		this.animalManager = new AnimalManager(entityManager);
		this.medicalPersonnelManager = new MedicalPersonnelManager(entityManager);
		this.appointmentManager = new AppointmentManager(entityManager, animalManager, medicalPersonnelManager);
		this.ownerManager = new OwnerManager(entityManager);
		this.historyManager = new HistoryManager(entityManager);
		this.userManager = new UserManager(entityManager);
	}
	
	private static Connection establishConnectionHelper() {
		Connection connection = null;
		try {
			connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/petshop", "root", "1q2w3e");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}
	
	public static void establishConnection() {
		connection = establishConnectionHelper();
	}
	
	public static Connection getConnection() {
		return connection;
	}

	public static void setConnection(Connection connection) {
		DatabaseUtil.connection = connection;
	}

	/* Delete */
	public interface Delete<T>{
		T executeAction(T t);
	}
	
	/* Find */
	public interface Find<T>{
		T executeAction(int id);
	}
	
	/*>=====< MANAGERS >=====<*/
	public AnimalManager manageAnimal() {
		return this.animalManager;
	}
	
	public MedicalPersonnelManager managePersonnelMedical() {
		return this.medicalPersonnelManager;
	}
	
	public AppointmentManager manageAppointment() {
		return this.appointmentManager;
	}
	
	public OwnerManager manageOwner() {
		return this.ownerManager;
	}
	
	public HistoryManager manageHistory() {
		return this.historyManager;
	}
	
	public UserManager manageUser() {
		return this.userManager;
	}
	
	/*>=====< CONNECTIONS METHODS >=====<*/ 
	public void startTransaction() {
		entityManager.getTransaction().begin();
	}
	
	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}
	
	public void closeEntityManager() {
		entityManager.close();
	}
	
	/*>=====< CONSOLE DISPLAY >=====<*/
	
	
	/**
	 * @param list A list that need to be displayed and could have any type.
	 */
	public <T> void printList(List<T> list) {
		for(T elem : list) {
			System.out.println("Elem: " + (String)elem);
		}
	}
	
	private interface PrintMedicalPersonnel {
		String executeAction(Personalmedical person);
	}
	
	private interface PrintProgramare {
		String executeAction(Programare programare);
	}
	
	public void printAllAnimalsFromDB() {
		List<Animal> results = entityManager.createNativeQuery("Select * from petshop.animal", Animal.class).getResultList();
		for( Animal animal : results) {
			System.out.println("Animal :" + animal.getName() + " has ID: " + animal.getIdAnimal());
		}
	}
	
	public void printAllPersonalMedicalFromDB() {
		List<Personalmedical> results = entityManager.createNativeQuery("Select * from petshop.personalmedical", Personalmedical.class).getResultList();
		
		/* Lambda used to display the Medical Personnel */
		PrintMedicalPersonnel print = (personalMedical) -> {
			return "Personal Medical :" + personalMedical.getName() + " has ID: " + personalMedical.getIdpersonalmedical() + " and Function: " + personalMedical.getJob();
		};
		
		for( Personalmedical personalMedical : results) {
			System.out.println(print.executeAction(personalMedical));
		}
	}
	
	public void printAllProgramareFromDB() {
		List<Programare> results = entityManager.createNativeQuery("Select * from petshop.programare", Programare.class).getResultList();
		
		/* Lambda used to display the Appointment */
		PrintProgramare print = (programare) -> {
			return "Programare :" + programare.getData().toString();
		};
		
		for( Programare programare : results) {
			System.out.println(print.executeAction(programare));
		}
	}

	interface Printing {
		String printProgramare(Programare programare);
	}
	
	public void printSortedProgramareFromDB() {
		List<Programare> results = entityManager.createNativeQuery("Select * from petshop.programare", Programare.class).getResultList();
		
		//results.sort(Comparator.comparing(Programare::getData));
		
		/* Lambda for sorting */
		Collections.sort(results, (appointment1, appointment2) -> appointment1.getData().compareTo(appointment2.getData()));
		
		Printing print = (prog) -> "Programare: " + prog.getData().toString() + " | " + "Animal: " + prog.getAnimal().getName() + " | " + "Personal Medical: " + prog.getPersonalmedical().getName();
		
		for( Programare programare : results) {
			System.out.println(print.printProgramare(programare));
		}
	}
}

