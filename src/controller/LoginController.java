package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.mysql.jdbc.Connection;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import model.User;
import socket.Echoer;
import util.DatabaseUtil;

/**
 * Login Window controller class that obseve the behavior of the user.
 * 
 * @author Ilie Florian-Cristian
 * @version 1.0
 * 
 */
public class LoginController implements Initializable {

	private Socket socket;
	private BufferedReader serverResponse;
	private PrintWriter loginDataProvider;
	
	private Echoer server;
	  ServerSocket serverSocket;
	
	@FXML
	private TextField loginUsername;
	@FXML
	private PasswordField loginPassword;
	@FXML
	private Label loginMessage;
	
	@FXML
	private TextField registerUsername;
	@FXML
	private PasswordField registerPassword;
	@FXML
	private PasswordField registerRePassword;
	@FXML
	private Label registerMessage;
	
	
	private Connection establishConnection() {
		Connection connection = null;
		try {
			connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/petshop", "root", "1q2w3e");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}
	
	@FXML
	public void onLoginPressed(ActionEvent event) {
		
		User currentUser = DatabaseUtil.getInstance().manageUser().checkAccount(loginUsername.getText(), loginPassword.getText());
		
		 /* Socket */
		try {
			serverSocket = new ServerSocket(232);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		/*
		try {
	    	while(true){
				server = new Echoer(serverSocket.accept());
				server.setUsername(currentUser.getUsername());
				server.setPassword(currentUser.getPassword());
				server.run();
	    	}
	    } catch (IOException e) {
			System.out.println(e.getMessage());
		}
		*/
		
		if ( currentUser != null ) {
			try {
				Parent root = FXMLLoader.load(getClass().getResource("/fxml/GUI.fxml"));
		        Scene scene = new Scene(root);
		        Stage stage = new Stage();
		        stage.setTitle("PetApp");
		        stage.setScene(scene);
		        stage.show();
		        ((Node)(event.getSource())).getScene().getWindow().hide();
		    } catch (IOException e) {
		    	e.printStackTrace();
		    }
		}
		else {
			loginMessage.setVisible(true);
			loginMessage.setTextFill(Color.rgb(210, 39, 30));
			
			loginUsername.setText("");
			loginPassword.setText("");
		}
	}
	
	@FXML
	public void onRegisterPressed(ActionEvent event) {
		
		User currentUser = DatabaseUtil.getInstance().manageUser().findUserByUsername(loginUsername.getText());
		
		if ( currentUser == null && registerPassword.getText().equals(registerRePassword.getText())) {
			
			DatabaseUtil.getInstance().manageUser().create(DatabaseUtil.getInstance().manageUser().getLastID(), registerUsername.getText(), registerPassword.getText(), 1);
			DatabaseUtil.getInstance().commitTransaction();
			
			try {
				Parent root = FXMLLoader.load(getClass().getResource("/fxml/GUI.fxml"));
		        Scene scene = new Scene(root);
		        Stage stage = new Stage();
		        stage.setTitle("PetApp");
		        stage.setScene(scene);
		        stage.show();
		        ((Node)(event.getSource())).getScene().getWindow().hide();
		    } catch (IOException e) {
		    	e.printStackTrace();
		    }
		}
		else if ( !registerPassword.getText().equals(registerRePassword.getText()) ) {
			registerMessage.setVisible(true);
			registerMessage.setLayoutX(registerMessage.getLayoutX() - 4);
			registerMessage.setText("The passwords doesn't match!");
			registerMessage.setTextFill(Color.rgb(210, 39, 30));
			
			registerUsername.setText("");
			registerPassword.setText("");
			registerRePassword.setText("");
		}
		else {
			registerMessage.setVisible(true);
			registerMessage.setLayoutX(registerMessage.getLayoutX() + 4);
			registerMessage.setText("The username already exists!");
			registerMessage.setTextFill(Color.rgb(210, 39, 30));
			
			registerUsername.setText("");
			registerPassword.setText("");
			registerRePassword.setText("");
		}
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			DatabaseUtil.getInstance().setUp();
		} catch (Exception e) {
			e.printStackTrace();
		}
		DatabaseUtil.getInstance().startTransaction();
		
		/* Establish connection to the Database */
	    DatabaseUtil.establishConnection();
	    
	   
	    
	    
	    /* Initialize Prompt Text */
	    loginUsername.setPromptText("Username");
	    loginPassword.setPromptText("Password");
	    
	    registerUsername.setPromptText("Username");
	    registerPassword.setPromptText("Password");
	    registerRePassword.setPromptText("Re-type Password");
	    
	    loginMessage.setVisible(false);
	    registerMessage.setVisible(false);
	}
	
}
