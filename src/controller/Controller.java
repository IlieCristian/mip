package controller;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.Animal;
import model.Owner;
import util.DatabaseUtil;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Main Window controller class that obseve the behavior of the user.
 * 
 * @author Ilie Florian-Cristian
 * @version 1.0
 * 
 */
/**
 * @author LenovoProfesionall
 *
 */
/**
 * @author LenovoProfesionall
 *
 */
public class Controller implements Initializable{
	
	public static Stage stage;
	
	static private int currentAnimalId;
	static private int currentAppointmentId;

	/* Getters */
	public static int getCurrentAnimalId() {
		return currentAnimalId;
	}
	public static int getCurrentAppointmentId() {
		return currentAppointmentId;
	}

	/* Setters */
	public static void setCurrentAnimalId(int currentAnimalId) {
		Controller.currentAnimalId = currentAnimalId;
	}
	public static void setCurrentAppointmentId(int currentAppointmentId) {
		Controller.currentAppointmentId = currentAppointmentId;
	}

	@FXML
	private Label appointmentTypeLabel;
	
	/* ANIMAL DATA */
	@FXML
	private TextField animalName;
	@FXML
	private TextField animalAge;
	@FXML
	private TextField animalWeight;
	@FXML
	private TextField animalSpecies;
	@FXML
	private TextField animalBreed;
	
	/* OWNER */
	@FXML
	private TextField ownerName;
	@FXML
	private TextField ownerPhoneNumber;
	@FXML
	private TextField ownerEmail;
	@FXML
	private ImageView profilePicture;
	
	/* LISTS */
	@FXML
	ListView<String> appointmentsList;
	ObservableList<String> appointmentsListItems = FXCollections.observableArrayList();
	@FXML
	ListView<String> medicalHistory;
	ObservableList<String> medicalHistoryItems = FXCollections.observableArrayList();
	
	
	/**
	 * This inserts the entities from Database Appointment list to the current list.
	 * @param connection The connection established with the database.
	 */
	private void insertAppointmentsInList(Connection connection) {
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = (Statement) connection.createStatement();
			resultSet = statement.executeQuery("SELECT * FROM programare");
	
			while (resultSet.next()) {
				appointmentsListItems.add(resultSet.getString(1) + " " + resultSet.getString(2));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * This inserts the entities from Database History list to the current list.
	 * @param connection The connection established with the database.
	 */
	private void insertHistoryInList(Connection connection) {
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = (Statement) connection.createStatement();
			resultSet = statement.executeQuery("SELECT * FROM history");
	
			while (resultSet.next()) {
				int databaseID = resultSet.getInt(5);
				if (databaseID == this.currentAnimalId) {
					medicalHistoryItems.add(resultSet.getString(1) + "  |  " + resultSet.getString(2) + "  |  " + resultSet.getString(3) + "  |  " + resultSet.getString(4));
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * This deletes the entities from Database History current list.
	 * @param connection The connection established with the database.
	 */
	private void deleteHistoryInList(Connection connection) {
		medicalHistoryItems.clear();
	}
	
	/**
	 * Useful function to generate a Date from a String:
	 * @param stringDate Date in string format.
	 * @return The Date generated from the given string.
	 * @throws ParseException
	 */
	public Date generateDate(String stringDate) throws ParseException {
		Date date = new Date();
		DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		date = formatter.parse(stringDate);
		return date;
	}
	
	/**
	 * Useful function to generate a Date from the given informations:
	 * @param year
	 * @param month
	 * @param date
	 * @param hourOfDay
	 * @param minute
	 * @return The Date generated from the given informations.
	 */
	public Date generateDate(int year, int month, int date, int hourOfDay, int minute) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, date, hourOfDay, minute);
		return calendar.getTime();
	}
	
	/**
	 * Generate the current date.
	 * @return The current date.
	 */
	public static Date getCurrentDate() {
	    Date date = new Date();
	    String strDateFormat = "hh:mm:ss a";
	    DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
	    return date;
	}
	
	/**
	 * Signaled when the Done button is pressed.
	 * @param event
	 * @throws Exception
	 */
	@FXML
	public void onDonePress(ActionEvent event) throws Exception {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/fxml/Done.fxml"));
	        Scene scene = new Scene(root);
	        Stage stage = new Stage();
	        stage.setTitle("PetApp");
	        stage.setScene(scene);
	        stage.show();
	        ((Node)(event.getSource())).getScene().getWindow().hide();
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
		
	}
	
	/**
	 * Signaled when the Cancel button is pressed.
	 * @param event
	 */
	@FXML
	public void onCancelPress(ActionEvent event) { 
		((Node)(event.getSource())).getScene().getWindow().hide();
	}
	
	/**
	 * Signaled when one of the list item is pressed.
	 * @param event
	 * @throws FileNotFoundException
	 */
	@FXML
	public void onListItemClicked(MouseEvent event) throws FileNotFoundException {
	    int appointmentId = Character.getNumericValue(appointmentsList.getSelectionModel().getSelectedItem().charAt(0));
	    currentAppointmentId = appointmentId;
	    
	    Animal animal = DatabaseUtil.getInstance().manageAppointment().read(appointmentId).getAnimal();
	    animalName.setText(animal.getName());
	    animalAge.setText(((Integer)animal.getAge()).toString());
	    animalWeight.setText(((Float)animal.getWeight()).toString());
	    animalSpecies.setText(animal.getSpecies());
	    animalBreed.setText(animal.getBreed());
	    this.currentAnimalId = animal.getIdAnimal();
	    
	    /* Read from file */
	    HashMap<String, String> imagePath = new HashMap<String, String>();
	    
	    imagePath.put("Labrador", "labrador");
	    imagePath.put("Chihuahua", "chihuahua");
	    imagePath.put("British Shorthair", "british_shorthair");
	    imagePath.put("Shiba Inu", "shiba_inu");

	    FileInputStream inputstream = new FileInputStream("D:\\Projects\\Eclipse Jee Projects\\PetShop\\src\\images\\" + imagePath.get(animal.getBreed()) + ".jpg"); 
	    Image image = new Image(inputstream); 
    	profilePicture.setImage(image);
	    
	    Owner owner = DatabaseUtil.getInstance().manageOwner().read(animal.getIdOwner());
	    ownerName.setText(owner.getName());
	    ownerPhoneNumber.setText(owner.getPhoneNumber());
	    ownerEmail.setText(owner.getEmail());
	    
	    deleteHistoryInList(DatabaseUtil.getConnection());
	    insertHistoryInList(DatabaseUtil.getConnection());
	}
	
	/**
	 * Signaled when one of the History item is pressed.
	 * @param event
	 */
	@FXML
	public void onHistoryItemClicked(MouseEvent event) { 
		/* Do nothing */
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			DatabaseUtil.getInstance().setUp();
		} catch (Exception e) {
			e.printStackTrace();
		}
		DatabaseUtil.getInstance().startTransaction();
		
		appointmentsList.setItems(appointmentsListItems);
		medicalHistory.setItems(medicalHistoryItems);
		
		/* Establish connection to the Database */
	    DatabaseUtil.establishConnection();
		
	    /* Populate the appointment list with data from the established Database */
		insertAppointmentsInList(DatabaseUtil.getConnection());
		insertHistoryInList(DatabaseUtil.getConnection());
		
		/* Listener for the Appointment List */
		appointmentsList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue != null) {
					System.out.println("Appointment: " + newValue);
				}
			}
        });
		
		/* Listener for the Medical History */
		medicalHistory.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue != null) {
					System.out.println("Medical History: " + newValue);
				}
			}
			
		});
		
		currentAnimalId = 0;
		currentAppointmentId = 0;
	}
}