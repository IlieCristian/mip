package controller;

import java.io.IOException;
import java.net.URL;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import com.mysql.jdbc.Connection;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Animal;
import model.History;
import util.DatabaseUtil;

/**
 * Done Window controller class that obseve the behavior of the user.
 * 
 * @author Ilie Florian-Cristian
 * @version 1.0
 * 
 */
public class DoneController implements Initializable {
	
	@FXML
	private TextField diagnosis;
	@FXML
	private TextField treatment;
	
	@FXML
	public void onDonePressed(ActionEvent event) throws IOException {
		
		History history = new History();
		DatabaseUtil.getInstance().manageHistory().create(DatabaseUtil.getInstance().manageHistory().getLastID(), Controller.getCurrentDate(), diagnosis.getText(), treatment.getText(), Controller.getCurrentAnimalId());	
		DatabaseUtil.getInstance().manageAppointment().delete(Controller.getCurrentAppointmentId());	
		DatabaseUtil.getInstance().commitTransaction();
	
		Parent root = FXMLLoader.load(getClass().getResource("/fxml/GUI.fxml"));
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setTitle("PetApp");
        stage.setScene(scene);
        stage.show();
		((Node)(event.getSource())).getScene().getWindow().hide();
	}
	
	@FXML
	public void onCancelPressed(ActionEvent event) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource("/fxml/GUI.fxml"));
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setTitle("PetApp");
        stage.setScene(scene);
        stage.show();
		
		((Node)(event.getSource())).getScene().getWindow().hide();
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		try {
			DatabaseUtil.getInstance().setUp();
		} catch (Exception e) {
			e.printStackTrace();
		}
		DatabaseUtil.getInstance().startTransaction();
		
		/* Establish connection to the Database */
	    DatabaseUtil.establishConnection();
		
	}

}
