package manager;

import javax.persistence.EntityManager;

import model.User;
import util.DatabaseUtil.Delete;
import util.DatabaseUtil.Find;

/**
 * The UserManager class manage the CRUD operations of the User table
 * made between the java program and the MySQL database
 * 
 * @author Ilie Florian-Cristian
 * @version 1.0
 * 
 */
public class UserManager {
	private static EntityManager entityManager;
	
	public UserManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	/* Lambda for User */
	Delete<User> deleteUser = (entity) -> {
    	entityManager.remove(entity);
    	return entity;
    };
    
    Find<User> findUser = (id) -> {
    	return entityManager.find(User.class, id);
    };
	
	/**
	 * This method create a new User object.
	 * @param id The id of the new User.
	 * @param username The username bounded with the account of the new User.
	 * @param password The password bounded with the account of the new User.
	 * @param type The type bounded with the account of the new User.
	 * @return Nothing.
	 */
	public void create(int id, String username, String password, int type) {
		entityManager.persist(new User(id, username, password, type));
	}
	
	/**
	 * This method searches for an existing User object and returns it.
	 * @param id The id of the user.
	 * @return User founded.
	 */
	public User read(int id) {
		return findUser.executeAction(id);
	}
	
	/**
	 * This method searches for an existing User object and updates it.
	 * @param id The id of the User.
	 * @param username The username bounded with the account of the new User.
	 * @param password The password bounded with the account of the new User.
	 * @param type The type bounded with the account of the new User.
	 * @return Nothing.
	 */
	public void update(int id, String username, String password, int type)
	{
		User foundedUser = findUser.executeAction(id);
		foundedUser.setEqual(id, username, password, type);
	}
	
	/**
	 * This method searches for an existing User and removes it.
	 * @param id The id of the user.
	 * @return Nothing.
	 */
	public void delete(int id) {
	    User user = findUser.executeAction(id);
	    deleteUser.executeAction(user);
	}
	
	/*>=====< UTIL FUNCTIONS >=====<*/
	
	public User findUserByUsername(String username) {
		int index = 1;
		User user = new User();
		
		while( user != null ) {
			user = entityManager.find(User.class, index);
			if( user != null && user.getUsername().equals(username) ) {
				return user;
			}
			index++;
		}
		return null;
	}
	
	public User checkAccount(String username, String password) {
		int index = 1;
		User user = new User();
		
		while( user != null ) {
			user = entityManager.find(User.class, index);
			if( user != null && user.getUsername().equals(username) && user.getPassword().equals(password)) {
				return user;
			}
			index++;
		}
		return null;
	}
	

	public int getLastID() {
		int index = 1;
		User user = new User();
		
		while( user != null ) {
			user = entityManager.find(User.class, index);
			if( user == null ) {
				break;
			}		
			index++;
		}
		return index;
	}
}
