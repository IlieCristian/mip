package manager;

import java.util.Date;

import javax.persistence.EntityManager;

import model.Programare;
import util.DatabaseUtil.Delete;
import util.DatabaseUtil.Find;

/**
 * The AppointmentManager class manage the CRUD operations of the Appointment table
 * made between the java program and the MySQL database
 * 
 * @author Ilie Florian-Cristian
 * @version 1.0
 * 
 */
public class AppointmentManager {
	private static EntityManager entityManager;
	AnimalManager animalManager;
	MedicalPersonnelManager medicalPersonnelManager;

	public AppointmentManager(EntityManager entityManager, AnimalManager animalManager, MedicalPersonnelManager medicalPersonnelManager) {
		this.entityManager = entityManager;
		this.animalManager = animalManager;
		this.medicalPersonnelManager = medicalPersonnelManager;
	}
	
	/* Lambda for Programare */
    Delete<Programare> deleteProgramare = (entity) -> {
    	entityManager.remove(entity);
    	return entity;
    };
    Find<Programare> findProgramare = (id) -> {
    	return entityManager.find(Programare.class, id);
    };
    
    /* Programari */
	/**
	 * This method create a new Programare object.
	 * @param idProgramare The id of the new programare.
	 * @param idAnimal The id of an existing animal.
	 * @param idPersonalMedical The id of an existing personal medical.
	 * @param date The date on when the appointment will be.
	 * @return Nothing.
	 */
	public void create(int idProgramare, int idAnimal, int idPersonalMedical, Date date) {
		Programare programare = new Programare(idProgramare,  date);
		programare.setAnimal(animalManager.read(idAnimal));
		programare.setPersonalmedical(medicalPersonnelManager.read(idPersonalMedical));
		entityManager.persist(programare);
	}
	
	/**
	 * This method searches for an existing Programare object and returns it.
	 * @param id The id of the programare.
	 * @return Programare founded.
	 */
	public Programare read(int id) {
		return findProgramare.executeAction(id);
	}
	
	/**
	 * This method searches for an existing Programare object and updates it.
	 * @param idProgramare The id of the programare.
	 * @param idAnimal The id of the animal.
	 * @param idAnimal The id of the animal.
	 * @param date The new date of the programare.
	 * @return Nothing.
	 */
	public void update(int idProgramare, int idAnimal, int idPersonalMedical, Date date) {
		Programare programare = findProgramare.executeAction(idProgramare);
		programare.setAnimal(animalManager.read(idAnimal));
		programare.setPersonalmedical(medicalPersonnelManager.read(idPersonalMedical));
		programare.setData(date);
	}
	
	/**
	 * This method searches for an existing Programare and removes it.
	 * @param id The id of the programare.
	 * @return Nothing.
	 */
	public void delete(int id) {
		Programare programare = findProgramare.executeAction(id);
		deleteProgramare.executeAction(programare);
	}

}
