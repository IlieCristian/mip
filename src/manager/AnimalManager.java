package manager;

import javax.persistence.EntityManager;

import model.Animal;
import util.DatabaseUtil.Delete;
import util.DatabaseUtil.Find;

/**
 * The AnimalManager class manage the CRUD operations of the Animal table
 * made between the java program and the MySQL database
 * 
 * @author Ilie Florian-Cristian
 * @version 1.0
 * 
 */
public class AnimalManager {
	private static EntityManager entityManager;
	
	public AnimalManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	/* Generic for Animal */
	Delete<Animal> deleteAnimal = (entity) -> {
    	entityManager.remove(entity);
    	return entity;
    };
    
    Find<Animal> findAnimal = (id) -> {
    	return entityManager.find(Animal.class, id);
    };
	
	/* Animal */
	/**
	 * This method create a new Animal object.
	 * @param id The id of the new animal.
	 * @param name The name of the new animal.
	 * @return Nothing.
	 */
	public void create(int idAnimal, String name, int age, float weight, String species, String breed, byte gender, int idOwner) {
		entityManager.persist(new Animal(idAnimal, name, age, weight, species, breed, gender, idOwner));
	}
	
	/**
	 * This method searches for an existing Animal object and returns it.
	 * @param id The id of the animal.
	 * @return Animal founded.
	 */
	public Animal read(int id) {
		return findAnimal.executeAction(id);
	}
	
	/**
	 * This method searches for an existing Animal object and updates it.
	 * @param id The id of the animal.
	 * @return Nothing.
	 */
	public void update(int id, String name, int age, float weight, String species, String breed, byte gender, int idOwner)
	{
		Animal foundedAnimal = findAnimal.executeAction(id);
		foundedAnimal.setEqual(name, age, weight, species, breed, gender, idOwner);
	}
	
	/**
	 * This method searches for an existing Animal and removes it.
	 * @param id The id of the animal.
	 * @return Nothing.
	 */
	public void delete(int id) {
	    Animal animal = findAnimal.executeAction(id);
	    deleteAnimal.executeAction(animal);
	}
}
