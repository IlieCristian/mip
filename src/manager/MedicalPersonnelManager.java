package manager;

import javax.persistence.EntityManager;

import model.Personalmedical;
import util.DatabaseUtil.Delete;
import util.DatabaseUtil.Find;

/**
 * The MedicalPersonnelManager class manage the CRUD operations of the MedicalPersonnel table
 * made between the java program and the MySQL database
 * 
 * @author Ilie Florian-Cristian
 * @version 1.0
 * 
 */
public class MedicalPersonnelManager {
	private static EntityManager entityManager;
	
	public MedicalPersonnelManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	/* Lambda for Personal Medical */
    Delete<Personalmedical> deletePersonalMedical = (entity) -> {
    	entityManager.remove(entity);
    	return entity;
    };
    
    Find<Personalmedical> findPersonalMedical = (id) -> {
    	return entityManager.find(Personalmedical.class, id);
    };

	/* Personal Medical */
	/**
	 * This method create a new PersonalMedical object.
	 * @param id The id of the new personal medical.
	 * @param name The name of the new personal medical.
	 * @param functie The function of the new personal medical.
	 * @return Nothing.
	 */
	public void create(int id, String name, String job) {
		entityManager.persist(new Personalmedical(id, name, job));
	}
	
	/**
	 * This method searches for an existing PersonalMedical object and returns it.
	 * @param id The id of the personal medical.
	 * @return PersonalMedical founded.
	 */
	public Personalmedical read(int id) {
		return findPersonalMedical.executeAction(id);
	}
	
	/**
	 * This method searches for an existing PersonalMedical object and updates it.
	 * @param id The id of the personal medical.
	 * @param name The name of the personal medical.
	 * @param job The function of the personal medical.
	 * @return Nothing.
	 */
	public void update(int id, String name, String job) {
		Personalmedical personalMedical = findPersonalMedical.executeAction(id);
		personalMedical.setName(name);
		personalMedical.setJob(job);
	}
	
	/**
	 * This method searches for an existing PersonalMedical and removes it.
	 * @param id The id of the personal medical.
	 * @return Nothing.
	 */
	public void delete(int id) {
		Personalmedical personalMedical = findPersonalMedical.executeAction(id);
		deletePersonalMedical.executeAction(personalMedical);
	}
}
