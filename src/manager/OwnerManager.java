package manager;

import javax.persistence.EntityManager;

import model.Owner;
import util.DatabaseUtil.Delete;
import util.DatabaseUtil.Find;

/**
 * The OwnerManager class manage the CRUD operations of the Owner table
 * made between the java program and the MySQL database
 * 
 * @author Ilie Florian-Cristian
 * @version 1.0
 * 
 */
public class OwnerManager {
	private static EntityManager entityManager;

	public OwnerManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/* Lambda for Owner */
    Delete<Owner> deleteOwner = (entity) -> {
    	entityManager.remove(entity);
    	return entity;
    };
    Find<Owner> findOwner = (id) -> {
    	return entityManager.find(Owner.class, id);
    };
    
    /* Owner */
	/**
	 * This method create a new Owner object.
	 * @param idOwner The id of the new owner.
	 * @param name The name of the new owner.
	 * @param phoneNumber The phoneNumber of the new owner.
	 * @param email The email of the new owner.
	 * @return Nothing.
	 */
	public void create(int idOwner, String name, String phoneNumber, String email) {
		Owner owner = new Owner(idOwner, name, phoneNumber, email);
		entityManager.persist(owner);
	}
	
	/**
	 * This method searches for an existing Owner object and returns it.
	 * @param id The id of the owner.
	 * @return Owner founded.
	 */
	public Owner read(int id) {
		return findOwner.executeAction(id);
	}
	
	/**
	 * This method searches for an existing Owner object and updates it.
	 * @param idOwner The id of the owner.
	 * @param name The name of the owner.
	 * @param phoneNumber The new phone number of the owner.
	 * @param email The new email of the new owner.
	 * @return Nothing.
	 */
	public void update(int idOwner, String name, String phoneNumber, String email) {
		Owner owner = findOwner.executeAction(idOwner);
		owner.setEqual(name, phoneNumber, email);
	}
	
	/**
	 * This method searches for an existing Owner and removes it.
	 * @param id The id of the owner.
	 * @return Nothing.
	 */
	public void delete(int id) {
		Owner owner = findOwner.executeAction(id);
	    deleteOwner.executeAction(owner);
	}
	
}
