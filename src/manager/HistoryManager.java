package manager;

import java.util.Date;

import javax.persistence.EntityManager;

import model.History;
import model.Owner;
import model.User;
import util.DatabaseUtil.Delete;
import util.DatabaseUtil.Find;

/**
 * The HistoryManager class manage the CRUD operations of the History table
 * made between the java program and the MySQL database
 * 
 * @author Ilie Florian-Cristian
 * @version 1.0
 * 
 */
public class HistoryManager {
	private static EntityManager entityManager;
	
	public HistoryManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	 /* Lambda for History */
    Delete<History> deleteHistory = (entity) -> {
    	entityManager.remove(entity);
    	return entity;
    };
    Find<History> findHistory = (id) -> {
    	return entityManager.find(History.class, id);
    };
    
    /* History */
	/**
	 * This method create a new History object.
	 * @param id The id of the new history.
	 * @param date The date of the new history.
	 * @param diagnostic The diagnostic of the new history.
	 * @param treatment The email of the new history.
	 * @param idAnimal The animal that have this history.
	 * @return Nothing.
	 */
	public void create(int id, Date date, String diagnostic, String treatment, int idAnimal) {
		History history = new History(id, date, diagnostic, treatment, idAnimal);
		entityManager.persist(history);
	}
	
	/**
	 * This method searches for an existing History object and returns it.
	 * @param id The id of the history.
	 * @return History founded.
	 */
	public History read(int id) {
		return findHistory.executeAction(id);
	}
	
	/**
	 * This method searches for an existing History object and updates it.
	 * @param id The id of the new history.
	 * @param date The date of the new history.
	 * @param diagnostic The diagnostic of the new history.
	 * @param treatment The email of the new history.
	 * @param idAnimal The animal that have this history.
	 * @return Nothing.
	 */
	public void update(int id, Date date, String diagnostic, String treatment, int idAnimal) {
		History history = findHistory.executeAction(id);
		history.setEqual(id, date, diagnostic, treatment, idAnimal);
	}
	
	/**
	 * This method searches for an existing History and removes it.
	 * @param id The id of the history.
	 * @return Nothing.
	 */
	public void delete(int id) {
		History history = findHistory.executeAction(id);
	    deleteHistory.executeAction(history);
	}
	
	public int getLastID() {
		int index = 1;
		History history = new History();
		
		while( history != null ) {
			history = entityManager.find(History.class, index);
			if( history == null ) {
				break;
			}		
			index++;
		}
		return index;
	}

}
