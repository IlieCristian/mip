package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the owner database table.
 * 
 */
@Entity
@NamedQuery(name="Owner.findAll", query="SELECT o FROM Owner o")
public class Owner implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idOwner;

	private String email;

	private String name;

	private String phoneNumber;

	public Owner() {
	}
	
	public Owner(int idOwner, String name, String phoneNumber, String email) {
		this.setEqual(idOwner, name, phoneNumber, email);
	}
	
	public void setEqual(int idOwner, String name, String phoneNumber, String email) {
		this.setIdOwner(idOwner);
		this.setName(name);
		this.setPhoneNumber(phoneNumber);
		this.setEmail(email);
	}
	
	public void setEqual(String name, String phoneNumber, String email) {
		this.setName(name);
		this.setPhoneNumber(phoneNumber);
		this.setEmail(email);
	}

	public int getIdOwner() {
		return this.idOwner;
	}

	public void setIdOwner(int idOwner) {
		this.idOwner = idOwner;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}