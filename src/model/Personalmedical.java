package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the personalmedical database table.
 * 
 */
@Entity
@NamedQuery(name="Personalmedical.findAll", query="SELECT p FROM Personalmedical p")
public class Personalmedical implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idpersonalmedical;

	private String job;

	private String name;

	public Personalmedical() {
	}

	public Personalmedical(int id, String name, String job) {
		this.idpersonalmedical = id;
		this.name = name;
		this.job = job;
	}
	
	public int getIdpersonalmedical() {
		return this.idpersonalmedical;
	}

	public void setIdpersonalmedical(int idpersonalmedical) {
		this.idpersonalmedical = idpersonalmedical;
	}

	public String getJob() {
		return this.job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}