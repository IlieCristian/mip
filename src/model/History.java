package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the history database table.
 * 
 */
@Entity
@NamedQuery(name="History.findAll", query="SELECT h FROM History h")
public class History implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idhistory;

	@Temporal(TemporalType.DATE)
	private Date date;

	private String diagnostic;

	private int idAnimalHistory;

	private String treatment;

	public History() {
		
	}
	
	public History(int id, Date date, String diagnostic, String treatment, int idAnimal) {
		this.idhistory = id;
		this.date = date;
		this.diagnostic = diagnostic;
		this.treatment = treatment;
		this.idAnimalHistory = idAnimal;
	}
	
	public void setEqual(int id, Date date, String diagnostic, String treatment, int idAnimal) {
		this.idhistory = id;
		this.date = date;
		this.diagnostic = diagnostic;
		this.treatment = treatment;
		this.idAnimalHistory = idAnimal;
	}
	
	public int getIdhistory() {
		return this.idhistory;
	}

	public void setIdhistory(int idhistory) {
		this.idhistory = idhistory;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDiagnostic() {
		return this.diagnostic;
	}

	public void setDiagnostic(String diagnostic) {
		this.diagnostic = diagnostic;
	}

	public int getIdAnimalHistory() {
		return this.idAnimalHistory;
	}

	public void setIdAnimalHistory(int idAnimalHistory) {
		this.idAnimalHistory = idAnimalHistory;
	}

	public String getTreatment() {
		return this.treatment;
	}

	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}

}