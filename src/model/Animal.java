package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the animal database table.
 * 
 */
@Entity
@NamedQuery(name="Animal.findAll", query="SELECT a FROM Animal a")
public class Animal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idAnimal;

	private int age;

	private String breed;

	private byte gender;

	private int idOwner;

	private String name;

	private String species;

	private float weight;

	public Animal() {
	}
	
	public Animal(int idAnimal, String name, int age, float weight, String species, String breed, byte gender, int idOwner) {
		this.setEqual(idAnimal, name, age, weight, species, breed, gender, idOwner);
	}

	public void setEqual(int idAnimal, String name, int age, float weight, String species, String breed, byte gender, int idOwner) {
		this.setIdAnimal(idAnimal);
		this.setName(name);
		this.setAge(age);
		this.setWeight(weight);
		this.setSpecies(species);
		this.setBreed(breed);
		this.setGender(gender);
		this.setIdOwner(idOwner);
	}
	
	public void setEqual(String name, int age, float weight, String species, String breed, byte gender, int idOwner) {
		this.setName(name);
		this.setAge(age);
		this.setWeight(weight);
		this.setSpecies(species);
		this.setBreed(breed);
		this.setGender(gender);
		this.setIdOwner(idOwner);
	}

	public int getIdAnimal() {
		return this.idAnimal;
	}

	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getBreed() {
		return this.breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public byte getGender() {
		return this.gender;
	}

	public void setGender(byte gender) {
		this.gender = gender;
	}

	public int getIdOwner() {
		return this.idOwner;
	}

	public void setIdOwner(int idOwner) {
		this.idOwner = idOwner;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSpecies() {
		return this.species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public float getWeight() {
		return this.weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

}