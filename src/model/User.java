package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idUser;

	private String password;

	private int type;

	private String username;

	public User() {
	}

	public User(int id, String username, String password, int type) {
		this.idUser = id;
		this.username = username;
		this.password = password;
		this.type = type;
	}
	
	public void setEqual(int id, String username, String password, int type) {
		this.idUser = id;
		this.username = username;
		this.password = password;
		this.type = type;
	}
	
	public int getIdUser() {
		return this.idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}