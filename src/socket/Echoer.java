package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import model.User;
import util.DatabaseUtil;

public class Echoer extends Thread {
	private Socket socket;
	
	private String username;
	private String password;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		System.out.println("Setter " + username);
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		System.out.println("Setter " + password);
		this.password = password;
	}

	public Echoer(Socket socket) {
		this.socket = socket;
	}
	
	private boolean accountExists(String username, String password) {
		User currentUser = DatabaseUtil.getInstance().manageUser().checkAccount(username, password);
		
		if (currentUser != null) {
			return true;
		}
		return false;
	}
	
	@Override
	public void run() {
		try {
			BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
			
			System.out.println(username);
			System.out.println(password);
			
			if(accountExists(username, password)) {
				System.out.println("Account is valid!");
			} else {
				System.out.println("Account is NOT valid!");
			}
			
		}catch (IOException e) {
			 System.out.println("ERROR: " + e.getMessage());
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
